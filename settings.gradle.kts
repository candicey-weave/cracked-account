rootProject.name = "WeaveCrackedAccount"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://jitpack.io")
    }
}

include("Zenith-Core")