# Cracked Account
- A [Weave](https://github.com/Weave-MC) mod that allows you to use cracked accounts on Lunar Client.

<br>

## Usage
- Click the `Cracked Account` button in the Lunar Client's login screen.
- Press `F10` in the singleplayer menu, or the multiplayer menu to change the name.

<br>

## Installation
1. Download the [Cracked Account](#download) mod.
2. Download the corresponding version of [Zenith Core](https://gitlab.com/candicey-weave/zenith-core/-/releases) that is mentioned in the release notes.
3. Place the jars in your Weave mods folder.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`
4. Download [Weave Manager](https://github.com/exejar/Weave-Manager).

<br>

## Build
1. Clone the repository.
2. Run `./gradlew build` in the root directory.
3. The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.
- [Lunar Mapping Project](https://github.com/Lunar-Mapping-Project) - For the mappings.

<br>

## Download
- [Release](https://gitlab.com/candicey-weave/cracked-account/-/releases)

<br>

## License
- Cracked Account is licensed under the [GNU General Public License Version 3](https://gitlab.com/candicey-weave/cracked-account/-/blob/master/LICENSE).
