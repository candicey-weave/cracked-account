package com.gitlab.candicey.weavecrackedaccount

import com.gitlab.candicey.weavecrackedaccount.hook.*
import com.gitlab.candicey.zenithcore.enum.LogicalOperator
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.extension.hasString
import com.gitlab.candicey.zenithcore.extension.hasStrings
import com.gitlab.candicey.zenithcore.font.FontManager
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import com.gitlab.candicey.zenithcore.lunarClasses
import net.weavemc.loader.api.ModInitializer
import java.awt.Font

class WeaveCrackedAccountMain : ModInitializer {
    override fun preInit() {
        info("Loading WeaveCrackedAccount...")

        lunarAccountManagerClassName = lunarClasses
            .find { LunarAccountManagerHook.checkClass(it.methods) != null }
            ?.name
            ?: error("Failed to find LunarAccountManager class")

        lunarMainMenuClassName = lunarClasses
            .find { it.hasStrings("trailer", "wrapped") }
            ?.name
            ?: error("Failed to find LunarMainMenu class")

        val lunarMicrosoftClass = lunarClasses
            .find { it.hasStrings("exp", "iat") }
            ?: error("Failed to find LunarMicrosoftAccount class")
        lunarMicrosoftAccountClassName = lunarMicrosoftClass.name
        lunarAccountClassName = lunarMicrosoftClass.superName

        minecraftProfileClassName = lunarClasses
            .find { it.hasStrings("minecraftProfile", "id", "name") && !it.hasString("accessTokenExpiresAt") }
            ?.name
            ?: error("Failed to find MinecraftProfile class")


        val lunarMigrationGuiClassName = lunarClasses
            .find { it.hasString("You'll need to migrate to a Microsoft account to log in.") }
            ?.name
            ?: error("Failed to find LunarMigrationGui class")

        HookManagerHelper.hooks.add(
            LunarAccountManagerHook(lunarAccountManagerClassName),
            LunarMigrationGuiHook(lunarMigrationGuiClassName),
            LunarAccountHook(lunarAccountClassName),
            LunarMicrosoftAccountHook(lunarMicrosoftAccountClassName),
            GuiSelectWorldHook,
            GuiMultiplayerHook,
            SessionHook,
        )

        configManager.init()

        FontManager.registerFont("Verdana", 24, Font.BOLD)

        info("Loaded WeaveCrackedAccount!")
    }
}