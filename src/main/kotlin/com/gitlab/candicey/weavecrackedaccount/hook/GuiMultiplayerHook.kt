package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.zenithcore.util.callStatics
import net.minecraft.client.gui.GuiMultiplayer
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

object GuiMultiplayerHook : Hook("net/minecraft/client/gui/GuiMultiplayer") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStatics<GuiMultiplayerHook>(
            "keyTyped",
        )
    }

    @JvmStatic
    fun onKeyTyped(guiMultiplayer: GuiMultiplayer, char: Char, keyCode: Int) {
        GuiSelectWorldHook.onKeyTyped(keyCode)
    }
}