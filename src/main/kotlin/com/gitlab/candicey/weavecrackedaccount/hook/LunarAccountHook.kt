package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.*
import com.gitlab.candicey.weavecrackedaccount.accessTokenFieldName
import com.gitlab.candicey.weavecrackedaccount.info
import com.gitlab.candicey.weavecrackedaccount.minecraftProfileClassName
import com.gitlab.candicey.weavecrackedaccount.minecraftProfileFieldName
import com.gitlab.candicey.weavecrackedaccount.setAccountMethodName
import com.gitlab.candicey.zenithcore.extension.hasString
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.LabelNode
import java.util.UUID

class LunarAccountHook(lunarAccountClassName: String) : Hook(lunarAccountClassName) {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val fields = node.fields
        val methods = node.methods

        fields.add(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "isCracked",
                "Z",
                null,
                false
            )
        )

        val accessTokenFieldNode = fields.find { it.desc == "Ljava/lang/String;" }!!
//        val instantFieldNode = fields.find { it.desc == "Ljava/time/Instant;" }!!
        val minecraftProfileFieldNode = fields.find { it.desc == "L$minecraftProfileClassName;" }!!
//        val refreshTokenFieldNode = fields.filter { it.access and Opcodes.ACC_TRANSIENT != 0 && it.access and Opcodes.ACC_PRIVATE != 0 }[1]

        val loadMethodNode = methods.find { it.desc == "(Lcom/google/gson/JsonObject;)V" }!!
        val setHeadPictureMethodNode = methods.run {
            var passedAbstract = false
            find {
                if (it.access and Opcodes.ACC_ABSTRACT != 0) {
                    passedAbstract = true
                    return@find false
                }

                passedAbstract && it.access and Opcodes.ACC_ABSTRACT == 0
            }
        }!!
        val setAccountMethodNode = methods.find { it.hasString("Setting account '%s' as the current session.") }!!

        accessTokenFieldName = accessTokenFieldNode.name
        minecraftProfileFieldName = minecraftProfileFieldNode.name
        setAccountMethodName = setAccountMethodNode.name

        loadMethodNode.instructions.insert(asm {
            val labelNode = LabelNode()

            aload(0)
            getfield(node.name, "isCracked", "Z")
            ifne(labelNode)
            _return
            +labelNode
        })
        info("Injected code into ${node.name}#${loadMethodNode.name} (LunarAccount#load).")

        setHeadPictureMethodNode.instructions.run {
            val firstGetField = find { it.opcode == Opcodes.GETFIELD }!! as FieldInsnNode
            val firstAStore1 = find { it.opcode == Opcodes.ASTORE && (it as org.objectweb.asm.tree.VarInsnNode).`var` == 1 }!!
            insert(firstAStore1, asm {
                val labelNode = LabelNode()

                aload(1)
                invokestatic(internalNameOf<LunarAccountHook>(), "onSetHeadPicture", "(Ljava/lang/String;)Z")
                ifeq(labelNode)
                aload(0)
                getfield(firstGetField.owner, firstGetField.name, firstGetField.desc)
                areturn
                +labelNode
            })
        }
        info("Injected code into ${node.name}#${setHeadPictureMethodNode.name} (LunarAccount#setHeadPicture).")

        cfg.computeFrames()
    }

    companion object {
        @JvmStatic
        fun onSetHeadPicture(string: String): Boolean = runCatching { UUID.fromString(string) }.isFailure
    }
}