package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.info
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode

class LunarMicrosoftAccountHook(lunarMicrosoftAccountClassName: String) : Hook(lunarMicrosoftAccountClassName) {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val isAccessTokenValidMethod = node.methods.find { it.desc == "()Z" } ?: error("Failed to find isAccessTokenValid method")
        isAccessTokenValidMethod.instructions.insert(asm {
            val labelNode = LabelNode()

            aload(0)
            invokestatic(internalNameOf<LunarMicrosoftAccountHandler>(), "onIsAccessTokenValid", "(Ljava/lang/Object;)Z")
            ifeq(labelNode)
            iconst_1
            ireturn
            +labelNode
        })
        info("Injected code into ${node.name}#${isAccessTokenValidMethod.name} (LunarMicrosoftAccount#isAccessTokenValid).")

        cfg.computeFrames()
    }
}