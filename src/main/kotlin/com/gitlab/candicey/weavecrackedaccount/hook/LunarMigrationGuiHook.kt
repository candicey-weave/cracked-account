package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.changeAccountName
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.*

class LunarMigrationGuiHook(lunarMigrationGuiClassName: String) : Hook(lunarMigrationGuiClassName) {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.forEach { methodNode ->
            val instructions = methodNode.instructions

            if (methodNode.name == "init") {
                val ldcInsnNodes = instructions.filterIsInstance<LdcInsnNode>()

                val offset = 10f

                val ldc50 = ldcInsnNodes
                    .find { it.cst == 50f }
                    ?: error("Failed to find LDC 50.0F")
                ldc50.cst = 50f + offset

                val ldc100 = ldcInsnNodes
                    .find { it.cst == 100f }
                    ?: error("Failed to find LDC 100.0F")
                ldc100.cst = 100f + offset * 2

                return@forEach
            }

            instructions
                .filterIsInstance<LdcInsnNode>()
                .forEach {
                    val constant = it.cst
                    if (constant is String) {
                        when (constant) {
                            "https://aka.ms/MinecraftMigration" -> {
                                instructions.clear()
                                instructions.insert(asm {
                                    invokestatic(internalNameOf<LunarMigrationGuiHook>(), "onGetStartButtonClick", "()V")
                                    iconst_0
                                    ireturn
                                })
                            }

                            "getStarted" -> {
                                val previous = it.previous
                                val next = it.next
                                val nextNext = next.next
                                val nextNextNext = nextNext.next

                                instructions.insert(it, asm {
                                    ldc("Cracked Account")
                                })

                                instructions.remove(previous)
                                instructions.remove(it)
                                instructions.remove(next)
                                instructions.remove(nextNext)
                                instructions.remove(nextNextNext)
                            }

                            "You'll need to migrate to a Microsoft account to log in." -> {
                                it.cst = "Migrate to a Microsoft account or use a cracked account by clicking the button below."

                                resizeBox(instructions)
                            }
                        }
                    }
                }
        }
    }

    private fun resizeBox(instructions: InsnList) {
        val varInsnNodes = instructions.filterIsInstance<VarInsnNode>()
        val ldcInsnNodes = instructions.filterIsInstance<LdcInsnNode>()

        val fstore4 = varInsnNodes
            .find { it.opcode == Opcodes.FSTORE && it.`var` == 4 }
            ?: error("Failed to find fstore 4")

        val newBoxWidth = 475f

        var getWidthMethodInsnNode: AbstractInsnNode? = fstore4
        while (getWidthMethodInsnNode !is MethodInsnNode) {
            getWidthMethodInsnNode = getWidthMethodInsnNode?.previous ?: error("Failed to find getWidth instruction")
        }

        val ldc155 = ldcInsnNodes
            .find { it.cst == 155f }
            ?: error("Failed to find LDC 155.0F")
        ldc155.cst = newBoxWidth / 2


        val ldc310 = ldcInsnNodes
            .find { it.cst == 310f }
            ?: error("Failed to find LDC 310.0F")
        ldc310.cst = newBoxWidth
    }

    companion object {
        @JvmStatic
        fun onGetStartButtonClick() {
            runAsync {
                changeAccountName()
            }
        }
    }
}