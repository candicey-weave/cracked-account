package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.*
import com.gitlab.candicey.weavecrackedaccount.accountManagerInstance
import com.gitlab.candicey.weavecrackedaccount.addAccountMethodName
import com.gitlab.candicey.weavecrackedaccount.extension.fromCrackedAccountMod
import com.gitlab.candicey.weavecrackedaccount.info
import com.gitlab.candicey.weavecrackedaccount.lunarAccountClassName
import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodNode
import org.objectweb.asm.tree.VarInsnNode

class LunarAccountManagerHook(lunarAccountManagerClass: String) : Hook(lunarAccountManagerClass) {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val foundMethodNode = checkClass(node.methods) ?: error("Failed to find method node")

        val methods = node.methods
        val previousMethodNode = methods[methods.indexOf(foundMethodNode) - 1]
        val previousInsnList = previousMethodNode.instructions
        previousInsnList.clear()
        previousInsnList.insert(asm {
            invokestatic(internalNameOf<LunarAccountManagerHandler>(), "onCanGoOnline", "()Z")
            ireturn
        })
        info("Injected code into ${node.name}#${previousMethodNode.name} (AccountManager#canGoOnline).")

        val initMethodNode = methods.find { it.name == "<init>" } ?: error("Failed to find init method node")
        initMethodNode.instructions.insertBeforeReturn(asm {
            aload(0)
            invokestatic(internalNameOf<LunarAccountManagerHandler>(), "onInit", "(Ljava/lang/Object;)V")
        })
        info("Injected code into ${node.name}#${initMethodNode.name} (AccountManager#<init>).")

        val setCurrentAccountMethodNode = methods.find { it.desc == "(L${lunarAccountClassName};)V" }!!
        setCurrentAccountMethodNode.instructions.insert(asm {
            val labelNode = LabelNode()

            aload(1)
            invokestatic(internalNameOf<LunarAccountManagerHandler>(), "onSetCurrentAccount", "(Ljava/lang/Object;)Z")
            ifeq(labelNode)
            _return
            +labelNode
        })
        info("Injected code into ${node.name}#${setCurrentAccountMethodNode.name} (AccountManager#setCurrentAccount).")

        val (removeAccountMethodNode, addAccountMethodNode) = methods.filter { it.desc == "(L${lunarAccountClassName};)Z" }
        removeAccountMethodNode.instructions.insert(asm {
            aload(1)
            invokestatic(internalNameOf<LunarAccountManagerHandler>(), "onRemoveAccount", "(Ljava/lang/Object;)V")
        })
        addAccountMethodName = addAccountMethodNode.name
        info("Injected code into ${node.name}#${removeAccountMethodNode.name} (AccountManager#removeAccount).")

        cfg.computeFrames()
    }

    companion object {
        var initialised = false

        fun checkClass(methods: List<MethodNode>): MethodNode? {
            methods.forEach { methodNode ->
                if (methodNode.desc == "(Ljava/lang/Object;[Ljava/lang/Object;)V") {
                    val insnList = methodNode.instructions
                    val accountsLdc = insnList
                        .filterIsInstance<LdcInsnNode>()
                        .find { it.cst == "Accounts" }
                        ?: return@forEach

                    val next = accountsLdc.next as? VarInsnNode ?: return@forEach
                    if (next.opcode != Opcodes.ALOAD || next.`var` != 0) {
                        return@forEach
                    }

                    val nextNext = next.next as? VarInsnNode ?: return@forEach
                    if (nextNext.opcode != Opcodes.ALOAD || nextNext.`var` != 1) {
                        return@forEach
                    }

                    return methodNode
                }
            }

            return null
        }
    }
}