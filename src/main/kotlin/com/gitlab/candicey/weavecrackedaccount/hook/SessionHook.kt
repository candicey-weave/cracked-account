package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.zenithcore.extension.addField
import net.weavemc.loader.api.Hook
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

object SessionHook : Hook("net/minecraft/util/Session") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.addField(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "fromCrackedAccountMod",
                "Z",
                null,
                false
            )
        )
    }
}