package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.allCrackedAccountNames
import com.gitlab.candicey.weavecrackedaccount.lunarAccountClassName
import com.gitlab.candicey.zenithcore.util.reflectField

object LunarMicrosoftAccountHandler {
    @JvmStatic
    fun onIsAccessTokenValid(account: Any): Boolean {
        val name = reflectField(lunarAccountClassName, "username").get(account) as String

        return allCrackedAccountNames.contains(name)
    }
}