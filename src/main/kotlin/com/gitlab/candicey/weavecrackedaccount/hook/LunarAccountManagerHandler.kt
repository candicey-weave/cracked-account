package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.*
import com.gitlab.candicey.weavecrackedaccount.accountManagerInstance
import com.gitlab.candicey.weavecrackedaccount.allCrackedAccountNames
import com.gitlab.candicey.weavecrackedaccount.extension.fromCrackedAccountMod
import com.gitlab.candicey.weavecrackedaccount.lunarAccountClassName
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.util.reflectField

object LunarAccountManagerHandler {
    @JvmStatic
    fun onCanGoOnline(): Boolean {
        if (!LunarAccountManagerHook.initialised) {
            LunarAccountManagerHook.initialised = true

            if (mc.session?.token == "0") {
                createAccountObject()
            }
        }

        val session = mc.session ?: return false
        val token = session.token ?: return false

        if (token.isBlank()) {
            return false
        }

        if (token == "0") {
            return session.fromCrackedAccountMod
        }

        return true
    }

    @JvmStatic
    fun onInit(accountManager: Any) {
        accountManagerInstance = accountManager
    }

    @JvmStatic
    fun onSetCurrentAccount(account: Any): Boolean {
        val name = reflectField(lunarAccountClassName, "username").get(account) as String

        if (allCrackedAccountNames.contains(name)) {
            login(name)
            return true
        }

        return false
    }

    @JvmStatic
    fun onRemoveAccount(account: Any) {
        val name = reflectField(lunarAccountClassName, "username").get(account) as String

        allCrackedAccountNames.remove(name)
    }
}