package com.gitlab.candicey.weavecrackedaccount.hook

import com.gitlab.candicey.weavecrackedaccount.changeAccountName
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.lwjgl.input.Keyboard
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodNode

object GuiSelectWorldHook : Hook("net/minecraft/client/gui/GuiSelectWorld") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val methodNode = MethodNode(
            Opcodes.ACC_PUBLIC,
            "keyTyped",
            "(CI)V",
            null,
            null
        )

        methodNode.instructions.insert(asm {
            iload(2)
            invokestatic(internalNameOf<GuiSelectWorldHook>(), "onKeyTyped", "(I)V")
            _return
        })

        node.methods.add(methodNode)

        cfg.computeFrames()
    }

    @JvmStatic
    fun onKeyTyped(keyCode: Int) {
        if (keyCode == Keyboard.KEY_F10) {
            changeAccountName()
        }
    }
}