package com.gitlab.candicey.weavecrackedaccount

import com.gitlab.candicey.weavecrackedaccount.extension.fromCrackedAccountMod
import com.gitlab.candicey.weavecrackedaccount.gui.GuiChangeName
import com.gitlab.candicey.weavecrackedaccount.wrapper.MinecraftProfile
import com.gitlab.candicey.zenithcore.HOME
import com.gitlab.candicey.zenithcore.RANDOM
import com.gitlab.candicey.zenithcore.extension.setSession
import com.gitlab.candicey.zenithcore.helper.NotificationHelper
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.nameRegex
import com.gitlab.candicey.zenithcore.util.reflectClass
import com.gitlab.candicey.zenithcore.util.reflectField
import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zenithcore.util.runAsync
import net.minecraft.util.Session
import java.io.File
import java.util.UUID

internal fun info(message: String) = LOGGER.info("[WeaveCrackedAccount] $message")
internal fun warn(message: String) = LOGGER.warn("[WeaveCrackedAccount] $message")

internal fun resolveConfigPath(name: String) = File("$HOME/.weave/CrackedAccount/$name")

fun changeAccountName() {
    runAsync {
        mc.displayGuiScreen(GuiChangeName(mc.currentScreen))
    }
}

fun createAccountObject(name: String? = null) {
//    info("Creating account object for $name...")

    val formattedName = (name ?: configAccount.config.crackedAccountName ?: return).trim()

    if (name != formattedName) {
        info("Formatted name to $formattedName.")
    }

    val previousSession = mc.session ?: firstSession ?: error("Session is null.")

    lastAccountName = previousSession.username
    if (firstSession == null) {
        firstSession = previousSession
    }

    /*val minecraftProfile = MinecraftProfile(firstSession?.playerID ?: error("Session is null."), formattedName).getObject()

    val microsoftAccountClass = reflectClass(lunarMicrosoftAccountClassName)
    val microsoftAccountInstance = microsoftAccountClass.constructors.first().newInstance(generateRandomUuid())

    reflectField(lunarAccountClassName, "isCracked").set(microsoftAccountInstance, true)
    reflectField(lunarAccountClassName, "username").set(microsoftAccountInstance, formattedName)
    reflectField(lunarAccountClassName, minecraftProfileFieldName).set(microsoftAccountInstance, minecraftProfile)

    reflectMethod(lunarAccountManagerClassName, addAccountMethodName, reflectClass(lunarAccountClassName)).invoke(accountManagerInstance, microsoftAccountInstance)
    reflectMethod(lunarAccountClassName, setAccountMethodName).invoke(microsoftAccountInstance)*/

    login(formattedName)

//    allCrackedAccountNames.add(formattedName)

//    info("Loaded account object for $formattedName.")
}

fun login(name: String?, notify: Boolean = true) {
    info("Attempting to login with $name...")

    val formattedName = (name ?: configAccount.config.crackedAccountName ?: return).trim()

    if (!nameRegex.matches(formattedName)) {
        warn("Invalid name: $formattedName")
        if (notify) {
            NotificationHelper.showNotification(
                "Weave Cracked Account",
                "Invalid name: $formattedName",
                NotificationHelper.Icon.ERROR.image
            )
        }

        return
    }

    val previousSession = mc.session ?: firstSession

    val session = Session(
        formattedName,
        previousSession?.playerID ?: generateRandomUuid(),
        previousSession?.token ?: "0",
        (previousSession?.sessionType ?: Session.Type.MOJANG).name
    ).apply {
        fromCrackedAccountMod = true
    }

    mc.setSession(session)

    if (formattedName != configAccount.config.crackedAccountName) {
        configAccount.config.crackedAccountName = formattedName
        configAccount.writeConfig()
        info("Saved cracked account name to config.")
    }

    info("Logged in with ${mc.session?.username} successfully.")
    if (notify) {
        NotificationHelper.showNotification(
            "Weave Cracked Account",
            "Changed cracked account name to $formattedName."
        )
    }
}

internal fun generateRandomUuid(): String = UUID.randomUUID().toString().replace("-", "")