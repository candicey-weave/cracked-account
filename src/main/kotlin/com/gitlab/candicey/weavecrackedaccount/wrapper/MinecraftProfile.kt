package com.gitlab.candicey.weavecrackedaccount.wrapper

import com.gitlab.candicey.weavecrackedaccount.minecraftProfileClassName
import com.gitlab.candicey.zenithcore.util.reflectClass
import com.gitlab.candicey.zenithcore.util.reflectField

class MinecraftProfile(private val id: String, private val name: String) {
    fun getObject(): Any {
        val clazz = reflectClass(minecraftProfileClassName)
        val instance = clazz.newInstance()
        reflectField(clazz, "id").set(instance, id)
        reflectField(clazz, "name").set(instance, name)

        return instance
    }
}