package com.gitlab.candicey.weavecrackedaccount.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.util.Session

var Session.fromCrackedAccountMod: Boolean by ShadowField()