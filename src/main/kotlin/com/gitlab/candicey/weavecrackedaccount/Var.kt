package com.gitlab.candicey.weavecrackedaccount

import com.gitlab.candicey.weavecrackedaccount.config.AccountJson
import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import com.gitlab.candicey.zenithcore.font.FontManager
import net.minecraft.util.Session
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.awt.Font

internal val LOGGER: Logger by lazy { LogManager.getLogger("WeaveCrackedAccount") }

internal val configManager = ConfigManager(
    mutableMapOf(
        "account" to Config(resolveConfigPath("account.json"), AccountJson()),
    )
)

internal val configAccount: Config<AccountJson> by configManager.delegateConfig()

internal val gbFont by lazy { FontManager.getFont("Verdana",  24, Font.BOLD) }

internal lateinit var lunarAccountManagerClassName: String
internal lateinit var lunarMainMenuClassName: String
internal lateinit var lunarAccountClassName: String
internal lateinit var lunarMicrosoftAccountClassName: String
internal lateinit var minecraftProfileClassName: String

internal lateinit var accountManagerInstance: Any
internal lateinit var accessTokenFieldName: String
internal lateinit var minecraftProfileFieldName: String
internal lateinit var addAccountMethodName: String
internal lateinit var setAccountMethodName: String

internal var lastAccountName: String? = null
internal var firstSession: Session? = null
internal val allCrackedAccountNames: MutableSet<String> = mutableSetOf()