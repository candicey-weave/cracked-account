package com.gitlab.candicey.weavecrackedaccount.config

data class AccountJson(
    var crackedAccountName: String? = null,
)
