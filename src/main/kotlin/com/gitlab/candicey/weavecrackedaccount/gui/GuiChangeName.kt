package com.gitlab.candicey.weavecrackedaccount.gui

import com.gitlab.candicey.weavecrackedaccount.configAccount
import com.gitlab.candicey.weavecrackedaccount.createAccountObject
import com.gitlab.candicey.weavecrackedaccount.gbFont
import com.gitlab.candicey.zenithcore.child.GuiScreenChild
import com.gitlab.candicey.zenithcore.child.GuiTextFieldChild
import com.gitlab.candicey.zenithcore.extension.addButton
import com.gitlab.candicey.zenithcore.extension.drawCenteredString
import com.gitlab.candicey.zenithcore.returnIf
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.GuiTextField
import org.lwjgl.input.Keyboard

class GuiChangeName(private val parentScreen: GuiScreen) : GuiScreenChild() {
    private lateinit var nameTextField: GuiTextField

    private lateinit var backButton: GuiButton

    override fun initGui() {
        nameTextField = GuiTextFieldChild(
            501,
            fontRendererObj,
            width / 2 - 100,
            60,
            200,
            20
        ).apply {
            text = configAccount.config.crackedAccountName ?: ""
            maxStringLength = 16
            allowedCharactersMatcher = { char, _ -> char.isLetterOrDigit() || char == '_' }
        }

        backButton = GuiButton(
            401,
            width / 2 - 100,
            height - 30,
            200,
            20,
            "Back"
        )

        addButton(
            backButton,
        )

        addGuiTextField(
            nameTextField,
        )

        Keyboard.enableRepeatEvents(true)
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        drawDefaultBackground()
        gbFont.drawCenteredString("Change Cracked Account Name", width / 2, 20, 0xFFFFFFFF.toInt())

        super.drawScreen(mouseX, mouseY, partialTicks)
    }


    override fun actionPerformed(button: GuiButton?) {
        when (button ?: return) {
            backButton -> mc.displayGuiScreen(parentScreen)
        }
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            returnIf(nameTextField.text.isEmpty())

            mc.displayGuiScreen(parentScreen)
            return
        }

        super.keyTyped(char, keyCode)
    }

    override fun onGuiClosed() {
        createAccountObject(nameTextField.text)

        Keyboard.enableRepeatEvents(false)

        super.onGuiClosed()
    }
}