import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version ("1.8.0")
    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

group = "com.gitlab.candicey.weavecrackedaccount"
version = "0.2.1"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("com.google.code.gson:gson:2.10.1")

    compileOnly("com.github.Weave-MC:Weave-Loader:70bd82faa6")
    compileOnly("com.gitlab.candicey-weave:zenith-core:v1.3.6")
}

tasks.test {
    useJUnitPlatform()
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "16"
}

tasks.jar {
    val wantedJar = listOf<String>()
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}